## Installation

Clone the repository

    git clone https://daniel182@bitbucket.org/daniel182/test_lending_project.git

Change to the project directory

    cd test_lending_project

Run docker services and wait for it to install dependencies

    docker-compose up --build

You can now access the project at http://localhost:8000/