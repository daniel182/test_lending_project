from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import json

app = Flask(__name__)
CORS(app)

@app.route('/api/check_loan', methods=['POST'])
@cross_origin()
def check_loan():
    if request.method == "POST":
        response = request.get_json()
        if int(response['requested_amount']) > 50000:
            result = "DECLINED"
        elif int(response['requested_amount']) == 50000:
            result = "UNDECIDED"
        elif int(response['requested_amount']) < 50000:
            result = "APPROVED"
        return  result

if __name__ == "__main__":
    app.run(host='0.0.0.0',  debug=True, port=5000)